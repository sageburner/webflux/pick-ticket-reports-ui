FROM ubuntu:zesty

# Update Ubuntu
RUN apt-get update && apt-get -y upgrade

# Install curl
RUN apt-get install -y curl apt-transport-https

# Install Yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get -y install yarn

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
# Optional
RUN apt-get install -y build-essential


# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json .
# For npm@5 or later, copy package-lock.json as well
# COPY package.json package-lock.json ./

RUN yarn install

# Bundle app source
COPY . .

EXPOSE 8080
CMD [ "yarn", "run", "dev" ]
